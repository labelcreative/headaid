<head>
	<meta charset="utf-8">
	
	<?php tf('page_specific_tracking_script_opening_head'); ?>
	<?php // Google Chrome Frame for IE ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php wp_title(''); ?></title>
	
	<?php tfo('opening_header_code'); ?>
	

	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<?php get_template_part('inc/favicon'); ?>

	<script>
    WebFontConfig = {
        google: { families: [ 'Noto+Sans:400,700' ] } // put your google fonts in here 
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
          '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })(); 
	</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/ouibounce/0.0.11/ouibounce.min.js"></script>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>
	<?php tfo('closing_header_code'); ?>

	<?php tf('page_specific_tracking_script_bottom_of_head'); ?>

	<?php if(strpos($_SERVER['HTTP_HOST'], '8888') !== false): ?>
		<script type="text/javascript" src="http://localhost:48626/takana.js"></script>
	    <script type="text/javascript">
	        takanaClient.run({host: 'localhost:48626'});
	    </script>
	<?php endif; ?>

</head>