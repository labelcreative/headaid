<?php 
/*
Template Name: Launchboom Landing Page
*/

	get_header(); 
?>
<div class="bgi top-section-wrap top-section-wrap-full text-center">	
	<div class="cf mw-1250 posr z1">
		<div class="top-logo-wrap"><?php acf_image('logo', 'p1'); ?></div>
		<div class="cf top-section-tall">
		  	<div class="cf">
				<div class="top-content-inner">
					<div class="from-m-down-p1 top-content-inner-top-half">
						<h1 class="text-left bold h2 top-content-headline"><?php tf('headline'); ?></h1>
						<?php acf_image('mobile_background', 'ma from-m-down'); ?>
						<div class="top-content text-left mb1 pb05 normal-page"><?php tf('top_content'); ?></div>
					</div>
					<div class="top-content-red">
						<div class="top-content-red-inner">
							<div class="cf">
								<?php 
									if (get_field('button_or_shortcode') == 'Shortcode'): ?>
										<div class="email-wrap">
											<?php echo do_shortcode(get_field('shortcode')); ?>
										</div><?
									else:
										?><a href=" ?><?php tf('button_link'); ?>" class="<?php tf('button_class'); ?>"> <?php tf('button_text'); ?> </a> <?php
									endif;
								?>
							</div>
							<div class="top-cta-text mt1 text-left normal-page"><?php tf('cta_text'); ?></div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<div class="side-image from-m-up">
			<?php acf_image('background'); ?>
		</div>
	</div>
</div>
<div class="from-m-down home-mobile-wrap">
	<div class="home-mobile-content">
		<?php while (have_rows('sections')): the_row(); ?>
			<div class="home-mobile-item">
				<?php acf_sub_image('image'); ?>
				<div class="home-mobile-text">
					<p class="bold"><?php tsf('title'); ?></p>
					<p><?php tsf('text'); ?></p>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</div>
<?php get_template_part('inc/exit-intent-modal'); ?>
<?php get_footer(); ?>