<?php 
/*
Template Name: Launchboom Landing Page Side
*/

	get_header(); 
?>
<?php background('background', '.top-section-wrap'); ?>
<?php 
if (!empty(get_field('logo'))):
	acf_image('logo', 'ma mobile-logo from-m-down');
else:
?>
	<h1 class="text-left bold"><?php tf('name'); ?></h1>
<?php endif; ?>
<?php acf_image('mobile_background', 'from-m-down top-mobile-image ma'); ?>
<div class="bgi top-section-wrap top-section-wrap-full text-center">	
	<div class="cf posr z1">
		<div class="cf top-section-tall">
		  	<div class="cf">
				<div class="top-content-inner">
					<div class="from-m-up">
						<?php 
						if (!empty(get_field('logo'))):
							acf_image('logo', 'ma');
						else:
						?>
							<h1 class="text-left bold"><?php tf('name'); ?></h1>
						<?php endif; ?>
					</div>
					<div class="top-content text-left mb1 pb05 normal-page"><?php tf('top_content'); ?></div>
					<div class="cf">
						<?php 
							if (get_field('button_or_shortcode') == 'Shortcode'): ?>
								<div class="email-wrap">
									<?php echo do_shortcode(get_field('shortcode')); ?>
								</div><?
							else:
								?><a href="<?php tf('button_link'); ?>" class="<?php tf('button_class'); ?>"> <?php tf('button_text'); ?> </a> <?php
							endif;
						?>
					</div>
					<div class="top-cta-text mt1 text-left normal-page"><?php tf('cta_text'); ?></div>
				</div>	
			</div>
		</div>
	</div>
</div>
<?php get_template_part('inc/exit-intent-modal'); ?>
<?php get_footer(); ?>