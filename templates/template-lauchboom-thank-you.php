<?php 
/*
Template Name: Thank You
*/

	get_header(); 
?>
<?php background('image', '.thank-you-side-image'); ?>
<div class="cf thank-you-wrap">	
	<div class="cf thank-you-content">
		<div class="cf">
			<div class="top-content-inner">
				<h2 class="thank-you-headline"><?php tf('headline') ?></h2>
                <?php acf_image('image', 'from-m-down thank-you-mobile-image'); ?>
				<p class="bigger"><?php tf('subheadline'); ?></p>
				<div class="normal-page"><?php tf('text'); ?></div>
				<div class="cf text-center thank-you-buttons">
                    <div class="fourcol first">
                        <a href="#" class="button thank-you-button mt05 facebook"><?php tf('fb_button_text'); ?></a>
                    </div>
                    <div class="fourcol ">
                        <a href="#" class="button thank-you-button mt05 twitter"><?php tf('tw_button_text'); ?></a>
                    </div>
                    <div class="fourcol last">
                        <a href="#" class="button thank-you-button mt05 email black"><?php tf('email_button_text'); ?></a>
                    </div>
                </div>
			</div>		
		</div>
	</div>
	<div class="from-m-up bgi side-image thank-you-side-image"></div>
</div>
<script>
    jQuery(document).ready(function($) {
        $(function() {
            $('.facebook').on('click', function(e) {
            	e.preventDefault();
                var w = 580, h = 300,
                        left = (screen.width/2)-(w/2),
                        top = (screen.height/2)-(h/2);
                    
                    
                    if ((screen.width < 480) || (screen.height < 480)) {
                        window.open ('http://www.facebook.com/share.php?u=<?php the_field('fb_share_link'); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
                    } else {
                        window.open ('http://www.facebook.com/share.php?u=<?php the_field('fb_share_link'); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);   
                    }
            });
            
            $('.twitter').on('click', function(e) {
            	e.preventDefault();
                var loc = encodeURIComponent('<?php the_field('tw_share_link'); ?>'),
                        title = "<?php the_field('tw_share_text'); ?>",
                        w = 580, h = 300,
                        left = (screen.width/2)-(w/2),
                        top = (screen.height/2)-(h/2);
                        
                    window.open('http://twitter.com/share?text=' + title + '&url=' + loc, '', 'height=' + h + ', width=' + w + ', top='+top +', left='+ left +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
            });
            
            $('.email').on('click', function(e) {
            	e.preventDefault();
                var subject = '<?php the_field('email_subject'); ?>';
                var emailBody = "<?php the_field('email_text'); ?>";
                window.location = 'mailto:?subject=' + subject + '&body=' +   emailBody;
            });
        });
    });
</script>
<?php get_footer(); ?>