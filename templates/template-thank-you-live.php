<?php 
/*
Template Name: Thank You Live
*/

    if (isset($_GET["email"])){
        $email = $_GET["email"];
    }
    if (isset($_GET["source"])){
        $source = $_GET["source"];
    }
    if (isset($_GET["campaign"])){
        $campaign = $_GET["campaign"];
    }

    get_header(); 
?>
<?php $img = get_field('background'); ?>

<style>
    .thank-you-wrap {
        background-image: url('<?php echo $img['sizes']['mobile-bg']; ?>');
    }
    @media screen and (min-width: 700px){
        .thank-you-wrap {
            background-image: url('<?php echo $img['url']; ?>');
        }
    }
    
</style>
<div class="bgi thank-you-wrap thank-you-wrap--live">
    <div class="p1 thank-you-inner">
        <h1 class="text-center h2 thank-you-headline"><?php tf('headline'); ?></h1>
        
        <div id="main-content" class="cf mw-1140">
            <div class="mw-450">
                <h3 class="h4 mt0"><?php tf('subheadline'); ?></h3>
                <p class="bigger"><?php tf('text'); ?></p>
                <a href="<?php tf('url'); ?>?utm_medium=<?php if (!empty($email)): echo $email; endif; ?>&utm_source=<?php if (!empty($source)): echo $source; endif; ?>&utm_campaign=fb-ad<?php tf('tracking_parameters'); ?>" class="button button-bigger"><?php tf('button_text'); ?></a>
                <h4 class="countdown-text bold ttu"><?php tf('bottom_text'); ?></h4>
            </div>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function($) {
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.text(minutes + ":" + seconds);

            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }

    jQuery(function ($) {
        var set_minutes = 60 * <?php tf('minutes') ?>,
            display = $('#minutes');
        startTimer(set_minutes, display);
    });
});
</script>


<?php get_footer(); ?>