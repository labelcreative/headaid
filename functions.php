<?php

/*
Author: Launchboom
URL: htp://launchboom.com/

*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/launch.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once( 'library/launch.php' ); // if you remove this, launch will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
// require_once( 'library/news-post-type.php' ); 
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
require_once( 'library/admin.php' ); 
/*
4. library/acf.php
	- adding custom acf functions to make things a little easier
*/
require_once( 'library/acf.php' ); 

/*
5. Plugin includes
*/
// require_once( 'library/plugins.php' ); // if you remove this, launch will break

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'launch-thumb-600', 600, 300, true );


/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function launch_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'launchtheme' ),
		'description' => __( 'The first (primary) sidebar.', 'launchtheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
}

// // 1. customize ACF path
// add_filter('acf/settings/path', 'my_acf_settings_path');
 
// function my_acf_settings_path( $path ) {
 
//     // update path
//     $path = get_stylesheet_directory() . '/plugins/acf/';
    
//     // return
//     return $path;
    
// }
 

// // 2. customize ACF dir
// add_filter('acf/settings/dir', 'my_acf_settings_dir');
 
// function my_acf_settings_dir( $dir ) {
 
//     // update path
//     $dir = get_stylesheet_directory_uri() . '/plugins/acf/';
    
//     // return
//     return $dir;
    
// }
 

// // 3. Hide ACF field group menu item
// // add_filter('acf/settings/show_admin', '__return_false');

// // 4. Include ACF
// include_once( get_stylesheet_directory() . '/plugins/acf/acf.php' );