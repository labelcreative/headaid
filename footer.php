<footer class="footer p1 text-center" id="footer" role="contentinfo">
    <p class="source-org copyright ma">&copy; <?php echo date('Y'); ?> <a href="<?php bloginfo( 'url' ); ?>"><?php bloginfo( 'name' ); ?></a>.</p>
</footer>
            </div> <?php //end #container ?>
                <?php wp_footer(); ?>
        </div> <?php //end .page-wrap ?>
<?php tfo('closing_body_code'); ?>
    </body>
</html> <?php // end page. what a ride! ?>